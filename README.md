[[_TOC_]]

These are my personal notes on some machine learning techniques.
All informations are based on the [Books](#Books) and several lectures held by [Dr. Eric Hilebrandt](https://www.uni-frankfurt.de/46992247/Das_Team_von_Prof__Dr__Hartmut_Roskos), [Prof. Rudolf Mester](https://www.vsi.cs.uni-frankfurt.de/), [Prof. Matthias Kaschube](https://www.fias.science/en/neuroscience/research-groups/matthias-kaschube/), [Prof. Nils Bertschinger](https://www.fias.science/de/systemische-risiken/gruppen/nils-bertschinger/), [Prof. Gemma Roig](http://www.cvai.cs.uni-frankfurt.de/) and [Prof. Visvanathan Ramesh](http://www.ccc.cs.uni-frankfurt.de/)

# Files

The [Cheat Sheet](https://gitlab.com/ganymede/machine-learning/-/jobs/629581229/artifacts/raw/cheat-sheet.pdf) is a compact summary of the most used formulars and short explanaitions of important machine learning techniques.

The [Mindmap](ML1_Mindmap.pdf) shows the most important points of the cheat sheet in an ordered mindmap.


# Online Resources

## Books
* [Pattern Recognition and Machine Learning](https://www.microsoft.com/en-us/research/publication/pattern-recognition-machine-learning/) (Christopher Bishop)
* [Hands-On Machine Learning with Scikit-Learn and TensorFlow](https://www.oreilly.com/library/view/hands-on-machine-learning/9781491962282/) (Aurélien Géron) ([Jupyter Notebooks](https://github.com/ageron/handson-ml))
* [Deep Learning Book](https://www.deeplearningbook.org/) (Ian Goodfellow and Yoshua Bengio and Aaron Courville)
* [The Elements of Statistical Learning](http://www.web.stanford.edu/~hastie/ElemStatLearn/) (Trevor Hastie, Robert Tibshirani, Jerome Friedman)
* [Information Theory, Inference, and Learning Algorithms](http://www.inference.org.uk/mackay/itila/book.html) (David MacKay)
* [Computer Vision](http://www.computervisionmodels.com/) (Simon J.D. Prince)
* [Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/index.html)
* [Deep Learning: Methods and Applications](http://research.microsoft.com/pubs/209355/DeepLearning-NowPublishing-Vol7-SIG-039.pdf) (Li Deng, Dong Yu)

### Interesting Publications
* [What is a Transformer?](https://medium.com/inside-machine-learning/what-is-a-transformer-d07dd1fbec04) (Source: [Attention Is All You Need](https://arxiv.org/abs/1706.03762))
* [Understanding LSTM Networks](http://colah.github.io/posts/2015-08-Understanding-LSTMs/) - Explains the LSTM cells' inner workings, plus, it has interesting links in conclusion.

## Software
* Deep Learning Frameworks
  * [TensorFlow](https://www.tensorflow.org/)
    * [Keras](https://keras.io/)
  * [PyTorch](https://pytorch.org/)
  * [Caffe](http://caffe.berkeleyvision.org/)
* [Faceswap](https://faceswap.dev/)
* [spaCy: Industrial-strength NLP](https://github.com/explosion/spaCy)

### Pre-trained Models
* Platform for Pre-trained Models
  * [TensorFlow Hub](https://tfhub.dev/)
  * [Model Zoo](https://modelzoo.co/)
  * [Papers with code](https://paperswithcode.com)

## Datasets
* [Big Bad NLP Database](https://datasets.quantumstat.com)
* [Kaggle](https://www.kaggle.com/)
* [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/index.php)
* [Awesome Public Datasets](https://github.com/awesomedata/awesome-public-datasets)

## Python
* [Python Data Science Handbook](https://jakevdp.github.io/PythonDataScienceHandbook/)
* [Interactive Tools](https://github.com/Machine-Learning-Tokyo/Interactive_Tools)
* Example Scripts (Courses)
    * [Basics](https://github.com/madewithml/basics)
    * [Deep Learning Tutorials](http://deeplearning.net/tutorial/)
	* [Dive into Deep Learning](https://d2l.ai/) - An interactive deep learning book
    * [FastAI](https://www.fast.ai/)
    * [PyMLViz](https://github.com/PyMLVizard/PyMLViz)
    * [100 Days of Machine Learning Coding](https://github.com/Avik-Jain/100-Days-Of-ML-Code)
    * [UCL](https://geometry.cs.ucl.ac.uk/)
      * [CreativeAI: Deep Learning for Graphics Tutorial Code](https://github.com/smartgeometry-ucl/dl4g)
      * [Acquisition and Processing of 3D Geometry](https://github.com/smartgeometry-ucl/COMPM080-Tutorials-2020)
      * [3D Machine Learning](https://github.com/smartgeometry-ucl/3D-Machine-Learning)

### Python Documentation
* [Learn Python](https://www.learnpython.org/)
* Data Science
  * [Numpy](https://docs.scipy.org/doc/numpy/reference/) (Arrays, Matrices)
  * [Scipy](https://docs.scipy.org/doc/scipy/reference/index.html) (Filters, Stats, etc.)
  * [Pandas](http://pandas.pydata.org/pandas-docs/stable/) (DataFrames, Series)
  * [Scikit-learn](http://scikit-learn.org/stable/auto_examples/index.html)
* Plotting
  * [Matplotlib](https://matplotlib.org/gallery/index.html)
  * [Seaborn](http://seaborn.pydata.org/examples/index.html)
  * [Bokeh](https://bokeh.pydata.org/en/latest/docs/gallery.html) (interactive plots)
  * [bqplot](https://github.com/bqplot/bqplot) (interactive Jupyter Notebook plots)

## Documentation
* [Awesome lists](https://github.com/sindresorhus/awesome) / [Lists](https://github.com/jnv/lists)
  * [Awesome Python](https://github.com/vinta/awesome-python)
  * [the-book-of-secret-knowledge](https://github.com/trimstray/the-book-of-secret-knowledge)
  * [Cyber Skills](https://github.com/joe-shenouda/awesome-cyber-skills)
  * [Git Addons](https://github.com/stevemao/awesome-git-addons)
  * [Privacy](https://github.com/Igglybuff/awesome-piracy)
  * [Security](https://github.com/sbilly/awesome-security)
    * [Personal Security Checklist](https://github.com/Lissy93/personal-security-checklist)
  * [Awesome Data Science](https://github.com/academic/awesome-datascience#readme)
    * [Computer Science courses with video lectures](https://github.com/Developer-Y/cs-video-courses)
    * [Awesome Data Visualization](https://github.com/fasouto/awesome-dataviz#readme)
    * [Awesome Scalability](https://github.com/binhnguyennus/awesome-scalability#readme)
  * [Awesome Machine Learning (Python)](https://github.com/josephmisiti/awesome-machine-learning#python)
    * [Awesome Deep Learning](https://github.com/ChristosChristofidis/awesome-deep-learning#readme)
      * [Awesome Deep Learning Resources](https://github.com/guillaume-chevalier/awesome-deep-learning-resources#readme)
      * [Awesome Deep Learning for Music](https://github.com/ybayle/awesome-deep-learning-music)
      * [Awesome TensorFlow](https://github.com/jtoy/awesome-tensorflow#readme)
      * [Awesome - Most Cited Deep Learning Papers](https://github.com/terryum/awesome-deep-learning-papers)


# License
[![CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)  
The text is released under the [CC BY 4.0 license](https://creativecommons.org/licenses/by/4.0/), and code is released under the [MIT license](https://opensource.org/licenses/MIT).
