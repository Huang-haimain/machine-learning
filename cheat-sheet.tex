\documentclass[10pt,
				DIV=30,
				BCOR=-2mm,
				a4paper,
				twoside,
				twocolumn,
				11pt,
				%draft
				]{scrartcl}

\newcommand{\myTitle}{Compendium}
\newcommand{\myName}{Jonathan Pieper}
\newcommand{\myUni}{Uni Frankfurt}

\include{config}

\title{Machine Learning}
\author{\myName}
\subtitle{Cheat Sheet}
\date{\today}

\begin{document}
	\maketitle

\section{Information Theory}
	\begin{deffin}{Probability}%{Mean and Variance}\\
		\textbf{Ensemble}: Random Variable $X$ is a triple $(x, A_X, P_X)$ with \textit{outcome} $x$ of \textit{Alphabet} $A_X = \{a_i\}$ (possible values) having \textit{probabilities} $P_X = \{p_i\}$.
		$P(x=a_i) = P(a_i) = P(x)$: Marginal Probability;
		$p(x)$: Probability density\\
		\textbf{Properties}:
		\begin{align*}
		\sum_{x\in A_X} P(x) = 	\int_{-\infty}^\infty p(x)dx &= 1 &
		0 ≤ P(x) ≤ 1\,\forall x\in A_X
		\end{align*}
		\centering \textbf{Mean} $E[r]$
		\begin{align*}
		μ = E[r] &= \sum_{r=0}^N P(r)\cdot r = \int_{-\infty}^{\infty} r\cdot p(r)dr \\
		E[x+y] &= E[x] + E[y]
		\end{align*}
		\centering \textbf{Variance} $\mathrm{var}[r]$
		\begin{eqnarray*}
		σ^2 = \mathrm{var}[r] &=& E[(r - E[r])^2] \\
		&=& E[r^2] - (E[r])^2
		\end{eqnarray*}
		\centering \textbf{Covariance} $\mathrm{cov}[r]$
		\begin{eqnarray*}
			\mathrm{cov}[x,y] &=& E[(x - E[x])(y-E[y])] \\
			&=& E[xy] - E[x]E[y]
		\end{eqnarray*}
	\centering \textbf{Correlation} $\mathrm{cor}[r]$
		\begin{eqnarray*}
			\mathrm{cor}[x,y] &=& E[xy] = \mathrm{cov}[x,y] + E[x]E[y]
		\end{eqnarray*}
	\textbf{Chi-squared} $χ^2$: Measures the deviation of data from the expected value.
		\begin{align*}
		χ^2 &= \frac{(x - μ)^2}{σ^2}
		\end{align*}
	\end{deffin}
	\begin{tBox}
		%\begin{tabular}{lcr}
			\textbf{Sum Rule}
			$$P(x) = \sum_y P(x,y)$$
			\textbf{Product Rule}
			$$ P(x,y) = P(x|y) P(y) $$
%			\textbf{Bayes Theorem}
%			$$P(y|x) = \dfrac{P(x|y) P(y)}{P(x)} $$
		%\end{tabular}
	\end{tBox}
	\begin{deffin}{Independence}
		Two random variables $X$ and $Y$ are independent (sometimes
		written $X\perp Y$ ) if and only if
		\begin{align*}
		P(x,y) &= P(x) P(y)\\
		⇒ \mathrm{var}[x+y] &= \mathrm{var}[x] + \mathrm{var}[y]
		\end{align*}
	\end{deffin}
	\begin{theoremeT}{\textbf{Bayes Theorem}.}
		If $θ$ denotes the unknown parameters and $D$ denotes the data, the general equation:
		$$ P(θ | D) = \frac{P(D|θ) P(θ)}{P(D)} = \frac{P(D|θ) P(θ)}{\sum_{θ\in A_θ} P(D,θ)} $$
		is written
		$$ \textrm{posterior} = \frac{\textrm{likelihood} \times \textrm{prior}}{\textrm{evidence}} $$
	\end{theoremeT}
	\textbf{The likelihood principle}: given a generative model for data $d$ given parameters $θ$, $P(d|θ)$, and having observed a particular outcome $d_1$, all inferences and predictions should depend only on the function $P(d_1 | θ)$.
	\begin{deffin}{Shannon information content}
		$$ h(x) = \log_2 \frac{1}{P(x)} $$
	\end{deffin}
	\begin{deffin}{Entropy}
		$$ H(X) = \sum_{x\in A_X} P(x) \log_2 \frac{1}{P(x)} $$
	\end{deffin}
	\includegraphics[width=\linewidth]{img/entropy}
	\begin{dBox}\textbf{Kullback-Leibler-Divergence (relative entropy)}:
		$$ D_{KL} (P\|Q) = \sum_x P(x) \log \frac{P(x)}{Q(x)} $$
		$$ D_{KL} (P\|Q) ≥ 0 $$
	\end{dBox}
	\begin{deffin}{Mutual Information}
		$$I(X;Y) = D_{KL}(p(x,y)\|p(x)p(y)) ≥ 0$$
	\end{deffin}
	\includegraphics[width=\linewidth]{img/information}
	\begin{tBox}
		Gaussian / Normal Distribution
		\begin{align*}
		\mathcal{N}(x|μ,σ^2) &= (2πσ^2)^{-½} \exp\left(-\frac{(x-μ)^2}{2σ^2}\right)\\
		\mathcal{N}(\mathbf{x}|\mathbf{μ},\Sigma) &= (2π)^{-D/2} {\det{\Sigma}^{-½}} \\ & \exp\left(-½ (\mathbf{x}-μ)^T \Sigma^{-1} (\mathbf{x} - \mathbf{μ})\right)
		\end{align*}
		Log-Likelihood-Solution:
		\begin{align*}
		\ln P(x | μ,σ) &= - N \ln (\sqrt{2π}σ) - \sum_n \frac{(x_n - μ)^2}{2σ^2}\\
		&= - N \ln (\sqrt{2π}σ) - [N (μ - \bar{x})^2 + S]/(2σ^2)
		\end{align*}
		\begin{align*}
		μ_{ML} = \bar{x} &= \frac{1}{N} \sum_{n=1}^{N} x_n & 
		S &= \sum_{n=1}^N (x_n - μ)^2 &
		σ_{ML}^2 &= \frac{S}{N}
		\end{align*}
		\begin{align*}
		E[μ_{ML}] &= μ & E[σ_{ML}^2] &= \frac{N-1}{N} σ^2
		\end{align*}
	\end{tBox}	
\section{Decision Theory}
	\includegraphics[width=\linewidth]{img/me1}
	\begin{dBox}
	Try to minimize the cost function $$κ(V=s_j|X=s_k) = L_{kj}$$ equivalent to maximizing $$\mNode{fill=green!20}{map}{$p(X=s_i|Z=m_j)$} = \frac{\tikz[baseline=-.7ex]{\node[fill=cyan!20](ml){$p(Z=m_j|X=s_i)$};}p(X=s_i)}{p(Z=m_j)}$$
	Maximize \mNode{fill=green!20}{map2}{A-Posteriori} Probability (\textbf{MAP})\\
	Maximize \mNode{fill=cyan!20}{map2}{Likelihood} (\textbf{ML-Estimate})
	\end{dBox}
	\begin{dBox}Principle of \textbf{Occam’s razor}:
		Pick the simplest model that adequately explains the data. → Regularize!
	\end{dBox}
	\begin{tBox}
		Misclassification rate:
		\begin{align*}
		 p(\text{mistake}) &= \tikz[baseline=-.7ex]{\node[fill=red!20](misclas1){$p(x\in R_1, C_2)$};} + \tikz[baseline=-.7ex]{\node[fill=blue!20](misclas2){$p(x\in R_2, C_1)$};} \\
		 &= \int_{R_1} p(x, C_2) dx + \int_{R_2} p(x, C_1) dx
		\end{align*}
		Loss/Cost Matrix $L_{kj}$ with target rows and prediction cols defines the cost for misclassification. \textbf{Expected Loss (Risk)}:
		$$ \mathbb{E}[L] = \sum_k \sum_j \int_{R_j} L_{kj} p(x, C_k) dx $$
	\end{tBox}
	\includegraphics[width=.54\linewidth]{img/decision}%
	\includegraphics[width=.45\linewidth]{img/reject}
	\begin{eBox}
		\textbf{Bayes Classifier}:
		$$c_{opt} = \arg \max_{C_j} P(C_j | \mathbf{x}) = \frac{P(\mathbf{x}|C_j) P(C_j)}{P(\mathbf{x})} $$
		\textit{Likelihood Ratio} (Log odds):
		$$\Lambda (x) = \frac{p(x|C_1)}{p(x|C_2)} \stackrel{C_1}{\underset{C_2}{\gtrless}} \frac{p(C_2)}{p(C_1)}$$
		Minimizing Bayes Risk:
		$$\Lambda (x) = \frac{p(x|C_1)}{p(x|C_2)} \stackrel{C_1}{\underset{C_2}{\gtrless}} \frac{p(C_2)(κ_{21}-κ_{22})}{p(C_1)(κ_{12}-κ_{11})}$$
		\textbf{Naive Bayes}:
		\begin{itemize}
			\begin{minipage}{.49\linewidth}
				\item Independent Features
			\end{minipage}
			\begin{minipage}{.49\linewidth}
				\item $p(\mathbf{x}|C_j) = \prod_i p(x_i|C_j)$
			\end{minipage}
		\end{itemize}
		$$ c_{opt} = \arg\max_{C_j} p(C_j) \prod_i p(x_i|C_j) $$
		\begin{itemize}
			\begin{minipage}{.69\linewidth}
				\item Small/Slow Product operation
			\end{minipage}%
			\begin{minipage}{.3\linewidth}
				\item[$⇒$] Logarithm
			\end{minipage}
		\end{itemize}
	\end{eBox}
\section{Optimization}
	\begin{tBox}
		\textbf{Train Model}\\
		\textit{Cross-validation}
		\begin{itemize}
			\begin{minipage}{.65\linewidth}
				\item Split Data (eg. 10 folds)
				\item Training- vs. Test-Error
			\end{minipage}%
			\begin{minipage}{.35\linewidth}
				\item 70\% Training
				\item 20\% Test
				\item 10\% Validation
			\end{minipage}
		\end{itemize}
		\textit{GridSearch / Randomized Search}
		\begin{itemize}
			\item Define Grid of Hyperparameters
			\item \texttt{sklearn.model\_selection.GridSearchCV}
			\item RandSearch: random combinations (probability)
			\item more control over the computing budget
		\end{itemize}
	\end{tBox}
	\begin{cBox}
		\textbf{Transforming and selecting features}:
		\begin{itemize}
			\begin{minipage}{.49\linewidth}
			\item Dimension Reduction
			\item Faster training
			\end{minipage}%
			\begin{minipage}{.5\linewidth}
			\item Reducing noise
			\item Avoid over-fitting
			\end{minipage}
		\end{itemize}
		\textbf{Transformation (basis functions)} (Design Matrix):
		$$ϕ_n(x) → Φ = \begin{pmatrix}
		ϕ_0(x_1) & \cdots & ϕ_n(x_n)\\
		\vdots & \ddots & \\
		ϕ_0(x_n) & & ϕ_n(x_n)
		\end{pmatrix}$$
	\end{cBox}
	\begin{cBox}
		\textbf{The curse of dimensionality} (Bellman, 1961):
		\begin{minipage}{.4\linewidth}
		\begin{itemize}
			\item Prior Knowledge
			\item Smoother target
		\end{itemize}
		\end{minipage}%
		\begin{minipage}{.6\linewidth}
			\begin{itemize}
			\item Dimensionality reduction
			\item Filtering Features
			\end{itemize}
		\end{minipage}
		\textbf{Principal Component Analyisis (PCA)}
		\begin{minipage}{.55\linewidth}
			\begin{itemize}
				\item Signal representation
				\item Fitting linear subspace
			\end{itemize}
		\end{minipage}%
		\begin{minipage}{.45\linewidth}
			\begin{itemize}
				\item Simplifying Data
				\item Less variation
				\item Keep high variance
			\end{itemize}
		\end{minipage}
	\begin{itemize}
		\item Compute variance of each dimension.
		\item Sort Principal Components with decreasing variance.
		\item Eliminate less important Principal Components.
		\item Project $\mathbf{x} \in \mathds{R}^{N}$ onto the eigenvectors $\mathbf{b}_i$ corresponding to the largest eigenvalues $λ_i$ fo the covariance Matrix $\Sigma_x$.
		\item Minimum of mean-squared magnitude of approximation error.
		\item Singular Value Decomposition (SVD): $X → U\cdot \Sigma \cdot V^T$
		\item $V^T = (c_1, c_2, \ldots, c_N)$: principal components
	\end{itemize}
	\end{cBox}
	\includegraphics[width=.49\linewidth]{img/pca}%
	\includegraphics[width=.5\linewidth]{img/pca2}
	\begin{eBox}\textbf{Update Rules:}\\
		\textbf{Batch Normalization}:
		\begin{itemize}
			\begin{minipage}{.65\linewidth}
				\item center and normalize inputs
				\item estimate mean and variance
			\end{minipage}%
			\begin{minipage}{.34\linewidth}
				\item Mini-Batches
				\item scale and shift
			\end{minipage}
		\end{itemize}
		\begin{align*}
		μ_B &= \frac{1}{N} \sum_{i=1}^N x_i & 
		σ_B^2 &= \frac{1}{N} \sum_{i=1}^N (x_i - μ_B)^2\\
		x_i &= \frac{x_i - μ_B}{\sqrt{σ_B^2 - ε}} &
		z_i &= γ x_i + β
		\end{align*}
		\begin{itemize}
			\begin{minipage}{.69\linewidth}
				\item $ε\sim 10^{-3}$: division by zero
				\item $z_i$: BN output
			\end{minipage}%
			\begin{minipage}{.39\linewidth}
				\item $γ$: Scaling
				\item $β$: Shifting
			\end{minipage}
		\end{itemize}
		\textbf{Stochastic Gradient Descent} (SGD)
		$$w_{t+1} = w_t - \eta \nabla E(w) $$
		\textbf{Momentum Gradient Descent}
		\begin{align*}
		m_{t+1} &= βm_t + \eta \nabla E(w) &
		w_{t+1} &= w_t - m_{t+1}
		\end{align*}
		\textbf{Nesterov‘s Accelerated Gradient} (NAG)
		\begin{align*}
		m_{t+1} &= βm + \eta \nabla_w E(w + βm) &
		w_{t+1} &= w_t - m
		\end{align*}
		\textbf{AdaGrad \& {\color{red}RMSProp}} (Adaptive Gradient Descent)
		\begin{align*}
		s_{t+1} &= {\color{red}β} s_t + {\color{red}(1-β)}\nabla E(w) \otimes \nabla E(w)\\
		w_{t+1} &= w_t - \eta \frac{\nabla E(w)}{\sqrt{s_{t+1} + w}}
		\end{align*}
		\textbf{Adam} (Adaptive Momentum Estimation)
		\begin{align*}
		m_{t+1} &= β_1 m_t + (1-β_1) \eta \nabla E(w) & \text{Momentum}\\
		s_{t+1} &= β_2 s_t + (1-β_2)\nabla E(w) \otimes \nabla E(w) & \text{RMSProp}\\
		m_{t+1} &= \frac{m_{t+1}}{1+β_1^t} & \text{Scaling}\\
		s_{t+1} &= \frac{s_{t+1}}{1+β_1^t} & \text{start} \\
		w_{t+1} &= w_t - \eta \frac{m}{\sqrt{s + w}}
		\end{align*}
		\textbf{Batch-/Online-Learning}:\\
		Batch-Learning\hfill Online-Learning
		\begin{itemize}
			\begin{minipage}{.5\linewidth}
				\item Full-Batch
				\item using all data
				\item offline learning
				\item computing resources
			\end{minipage}%
			\begin{minipage}{.5\linewidth}
				\item incremental learning
				\item one data-point at a time
				\item fast and cheap
				\item small groups: \textit{mini-batch}
			\end{minipage}%
		\end{itemize}
	\end{eBox}
\section{Decision Trees}
%	\begin{cBox}
		\begin{itemize}
			\begin{minipage}{.5\linewidth}
				\item Not metric
				\item Heterogen Attributes
				\item Interpretable / Fast
				\item Pruning (Cutoff)
				\item Weakest link Pruning
				\item Complexity: $\sharp{}$Knots
			\end{minipage}%
			\begin{minipage}{.5\linewidth}
				\item CART (Classification and Regression Trees) (binary)
				\item C4.5 → C5.0 (not binary)
			\end{minipage}
		\end{itemize}
%	\end{cBox}
	\begin{tBox}
		\textbf{Bootstrapping}:
		\begin{itemize}
			\item Dataset: $Z = \{z_i^{(b)}\}_{i=1}^N$, $z_i(x_i)$
			\item Draw $B$ new bootstrapping sampling with replacement from Data $Z^{*(b)} = \{z_i^{(b)}\}_{i=1}^N$
			\item Calculate feature $S(Z)$ for bootstrapping sample:
			\begin{align*}
			\hat{S}^* &= \frac{1}{B} \sum_{b=1}^B S(Z^{*(b)}) \\
			\text{var}[S(Z)] &= \frac{1}{B-1} \sum_{b=1}^B (S(Z^{*(b)}) - \hat{S}^*)^2\\
			\hat{E}_{boot} &= \frac{1}{B} \frac{1}{N} \sum_{b=1}^B \sum_{i=1}^N L(y, f^{*(b)}(x_i))
			\end{align*}
		\end{itemize}
		\textbf{Bagging} (Bootstrap-Aggregation)
		\begin{itemize}
			\item Use Bootstrapping to improve prediction
			\item Take mean of Bootstrapping predictions
		\end{itemize}
		\textbf{Bumping} (Bootstrap sampling to move randomly through model
		space)\\
		\textbf{Random Forests} (Bagging with $B$ (idealized) iid Trees $⇒$ Reducing variance $σ^2/B$)
		\begin{itemize}
			\item Draw Bootstrap sample $Z^*$ of size $N$ from training data.
			\item Grow a random-forest tree $T_b$ to the bootstrapped data until the minimum node size $n_min$ is reached.
			\begin{itemize}
				\item Select $m$ variables at random from the $p$ variables.
				\item Pick the best variable/split-point among the $m$.
				\item Split Node into two daughter nodes.
			\end{itemize}
			\item Output the ensemble of Trees $\{T_b\}_1^B$.
		\end{itemize}
		\textit{Weak Learner/Regressor}: Error bit better than randomized guess.\\
		\textbf{Boosting}: combination of weak learners.\\
		\textbf{AdaBoost}: Classifiers are trained on weighted versions
		of the dataset, and then combined to produce a final prediction:
		\begin{enumerate}
			\item Initialize $w_i = 1/N$. For $m=1$ to $M$ do:
			\item Fit a classifier $G_m(x)$ to the training data using weights $w_i$.
			\item Compute $α_m = \log((1 - \text{err}_m)/\text{err}_m)$.
			\item Set $w_i \leftarrow w_i \cdot \exp[α_m \cdot I(y_i \neq  G_m(x_i))]$
			\item Output $G(x) = \text{sign}\left[\sum_m α_m G_m(x)\right]$
		\end{enumerate}
		For large $M$:
		\begin{itemize}
			\item Perfect classifier (if $M$ not too large)
			\item Test Error still reducing after Train-Error = 0
		\end{itemize}
	\end{tBox}
	\includegraphics[width=\linewidth]{img/trees}
\section{Linear Models}
	\begin{eBox}
		\textbf{Linear Regression}:
		$$y(x,w) = x^T \cdot w$$
		\textbf{Least Squares Error}:
		$$ E(w) = \sum_{n=1}^N (t_n - x^T \cdot w)^2 = (t - Xw)^T(t-Xw)$$
		$$ ⇒ w_{ML}= (X^T X)^{-1}(X^T t) $$
		\textbf{Regularization}:
		$$ \tilde{E}(w) = E(w) + \frac{λ}{2} \sum_j \|w_j\|_q^q $$
		$$q=2\text{ (L2)}\qquad ⇒ w_{ML} =  (λ\mathds{1} - X^T X)^{-1}(X^T t)  $$
		\textbf{Lasso} ($q=1$): \textit{only numeric} (single weights → 0).\\
		\textbf{Ridge} ($q=2$): analytic solution (minimize all weights).\\
		\textbf{Elastic-Net}: $λ(α\|w\|_2^2 + (1-α)\|w\|_1)$ (combination).
	\end{eBox}
	\begin{tBox}
	\textbf{Bayesian Linear Regression}:\\
	Gaussian Prior over parameters: $p(w|α) = \mathcal{N}(w|0,α^{-1}\mathds{1})$\\
	Posterior distribution: $p(w|t) = \mathcal{N}(w|m_N, S_N)$
	\begin{align*}
	m_N &= β S_N Φ^T t &
	S_N^{-1} &= α\mathds{1} + β Φ^TΦ
	\end{align*}
	Predictive distribution: $p(t|x) = \mathcal{N}(t|m_Nϕ(x),σ_N^2(x))$
	$$σ_N^2 (x) = β^{-1} + ϕ(x)^T S_N ϕ(x) $$
	\end{tBox}
%\section{Linear Classification}
	Use discriminant function $d_k(z)$ for each class $s_k\in V$,
	find maximum score ($\arg\max d_k(z)$) and check for rejection rule:
	\begin{cBox}
	$$ p(C_1|x) = \frac{p(x|C_1)p(C_1)}{p(x)} = \frac{1}{1+\exp(-a)} = σ(a)$$
	$$ a = \ln \frac{p(x|C_1)p(C_1)}{p(x|C_2)p(C_2)} $$
	\end{cBox}
	\includegraphics[width=.9\linewidth]{img/me2}
	\begin{dBox}
		\textbf{Sigmoid Function} $σ(a) = (1+\exp(-a))^{-1}$
		\begin{align*}
		σ(-a) &= 1- σ(a) & \frac{d}{da} σ &= σ(1-σ) &
		a &= \ln\left(\frac{σ}{1-σ}\right)
		\end{align*}
	\end{dBox}
	\includegraphics[width=.85\linewidth]{img/sigmoid}
	\begin{tBox}
		\textbf{Perceptron}:
		\begin{align*}
		y(x) &= \text{sign}(w^T ϕ)\\
		E_P(w) &= -\sum_{n\in\mathcal{M}} w^T ϕ_n t_n & \nabla E(w) &= - ϕ_n t_n
		\end{align*}
		\textbf{Logistic Regression}
		\begin{align*}
		p(C_1|ϕ) = y(ϕ) &= σ(w^Tϕ)\\
		p(t|w) &= \prod_{n=1}^N y_n^{t_n} (1-y_n)^{1-t_n}\\
		E(w) &= -\sum_{n=1}^N t_n \ln y_n + (1-t_n)\ln(1-y_n)\\
		\nabla E(w) &= \sum_{n=1}^N (y_n - t_n) ϕ
		\end{align*}
		Multiclass (Softmax):
		\begin{align*}
		p(C_k|ϕ) = y_k(ϕ) &= \frac{\exp(w_k^T ϕ)}{\sum_{j=1}^K \exp(w_j^T ϕ)}		
		\end{align*}
		\textbf{Linear Discriminant Analysis (LDA)} (Fishers linear discriminant)
		\begin{align*}
		E(w) &= \frac{\|w^T(μ_2 - μ_1)\|^2}{s_1^2 + s_2^2} & s_k^2 &= \sum_{n\in C_k} (y_n - μ_m)^2 
		\end{align*}
	\end{tBox}
	\includegraphics[width=\linewidth]{img/fisher}
	\subsection{Support Vector Machines (SVM)}
	\begin{dBox}\textbf{Discriminant Function}:
		Hyperplane which separates datasets (separating hyperplane):
		$$L:\quad f(x) = β_0 + β^Tx = 0$$
		\begin{enumerate}
			\item For any two points $x_1$ and $x_2$ lying in $L$, $β^T (x_1 - x_2) = 0$, and hence $β^* = β/\|β\|$ is the vector normal to the surface.
			\item For any point $x_0$ in $L$: $β^T x_0 = -β_0$.
			\item The signed distance of any point $x$ to $L$ is given by
			$$ β^{*T} = \frac{1}{\|β\|} (β^T x + β_0) = \frac{f(x)}{\|f'(x)\|} $$
		\end{enumerate}
	\end{dBox}
	\begin{cBox}
		Goal: minimize $D(β,β_0)$ for misclassified $x_i \in \mathcal{M}$
		$$ D(β,β_0) = -\sum_{i\in \mathcal{M}} y_i(x_i^β + β_0) $$
		or maximize the margin $M = 1/\|β\|$
		\begin{align*}
		\min \frac{1}{2} \|β\|^2 && \text{subject to } y_i(x_i^Tβ + β_0) ≥ 1
		\end{align*} 
		The Lagrange (primal) function, to be minimized w.r.t. $β$ and $β_0$, is
		$$ L_P = \frac{1}{2} \|β\|^2 - \sum_{i=1}^N α_i[y_i(x_i^Tβ + β_0) - 1] $$
		\begin{align*}
		β &= \sum_{i=1}^N α_i y_i x_i & 0 &= \sum_{i=1}^N α_i y_i
		\end{align*}
		$$ L_D = \sum_{i=1}^N α_i - \frac{1}{2}\sum_{i=1}^N \sum_{k=1}^N α_i α_k y_i y_k x_i^T x_k $$
		Where $α_i ≥ 0$ and $α_i[y_i(x_i^Tβ + β_0) - 1] = 0$.
		\begin{itemize}
			\item $α_i > 0$ ⇒
			$y_i(x_i^Tβ + β_0) = 1$ ($x_i$ is a support vector) 
			\item $α_i = 0$ ⇒
			$y_i(x_i^Tβ + β_0) > 1$ ($x_i$ \textbf{not} a support vector) 
		\end{itemize}
	\end{cBox}
	\includegraphics[width=.5\linewidth]{img/cls2}%
	\includegraphics[width=.49\linewidth]{img/svm2}
	\begin{cBox}
		Define the slack variables $ξ$ to deal with overlaps.\\
		$C$: "cost" parameter
		$$ \min \frac{1}{2}\|β\|^2 + C \sum ξ_i$$
		$$\text{subject to}\left\{ \begin{minipage}{4cm}$
		y_i (x_i^T β + β_0) ≥ 1 - ξ_i \\
		ξ_i ≥ 0$
		\end{minipage} \right.$$
		Results in Lagrange (primal) function
		\begin{align*}
		L_P = &\frac{1}{2}\|β\|^2 + C \sum ξ_i \\ 
		& - \sum α_i [y_i (x_i^T β + β_0) - (1 - ξ_i)] - \sum μ_i ξ_i
		\end{align*}
		which we minimize w.r.t $β$, $β_0$ and $ξ_i$:
		\begin{align*}
		β &= \sum α_i y_i x_i & 0 &= \sum α_i x_i & α_i &= C - μ_i
		\end{align*}
		$$ L_D = \sum α_i - \frac{1}{2} \sum_i \sum_j α_i α_j y_i y_j x_i^T x_j $$
		\textbf{Karush-Kuhn-Tucker} conditions:
		\begin{align*}
		 α_i [y_i (x_i^T β + β_0) - (1 - ξ_i)] &= 0 & 
		 μ_i ξ_i &= 0\\
		 y_i (x_i^T β + β_0) - (1 - ξ_i) &≥ 0
		\end{align*}
		Resulting function only depends on $x_i^T x_i$ (Support vectors):
		$$ f(x) = \sum_i α_i y_i x_i^T x_i + β_0 $$
	\end{cBox}
	\subsection{Kernel Trick}
	\begin{dBox}
		Kernel function defined by scalar product of basis functions:
		$$ K(x_i,x_j) = ϕ(x_i)^T ϕ(x_j) $$
		\begin{itemize}
			\item $d$th degree polynomial: $ K(x_i,x_j) = (1+\langle x_i,x_j\rangle)^d$
			\item Radial basis: $K(x_i, x_j) = \exp (-γ\|x_i - x_j\|^2)$
			\item Neural Network: $K(x_i, x_j) = \tanh (κ_1 \langle x_i, x_j \rangle + κ_2)$
		\end{itemize}
		Results in function
		$$ f(x) = \sum_i α_i y_i K(x, x_i) + β_0 $$
		and Classifier $G(x) = \text{sign}(f(x))$.
	\end{dBox}
	\includegraphics[width=.8\linewidth]{img/svm_nn}
\section{Neural Networks}
	\includegraphics[width=\linewidth]{img/nn2}
	\begin{tBox}
		\begin{align*}
		z_j &= h\left(\sum_i w_{ji}^{(1)} x_i\right) & y_k &= h_2\left(\sum_j w_{kj}^{(2)} z_j\right)\\
		z_j &= h(w_j^{(1)T} x) & y_k &= h_2(w_k^{(2)T} z)
		\end{align*}
		Activation: $a_j = \sum_i w_{ji}^{(1)} x_i$\\
		Activity / Output: $z_j = h(a_j)$\\
		Activation functions $h(a)$:
		\begin{itemize}
			\item Linear: $h(a) = a$.
			\item Logistic Sigmoid: $σ(a) = (1+e^{-a})^{-1}$
			\item Sigmoid: $h(a) = \tanh(a)$
			\item ReLU: $h(a) = \max(a,0)$
		\end{itemize}
	\end{tBox}
	\begin{eBox}
		\textbf{Backpropagation}
		\begin{align*}
		E(w) &=  \sum_{n=1}^N  E_n(w) =  \sum_{n=1}^N  \frac{1}{2}\sum_{k} (y_{nk} - t_{nk})^2\\
		\frac{∂E}{∂w_{ji}} &= \frac{∂E}{∂a_j} \frac{∂a_j}{∂w_{ji}} = 
		 \frac{∂E}{∂a_k} \frac{∂a_k}{∂a_{j}}\frac{∂a_j}{∂w_{ji}}
		\end{align*}
		\begin{itemize}
			\item Apply an input vector $x_n$ to the network and forward propagate through the network.
			\item Evaluate the $δ_k = (y_k - t_k)$ for all the output units.
			\item Backpropagate the $δ$’s to obtain $δ_j = h'(a_j) \sum_k w_{kj} δ_k$ for each hidden unit in the network.
			\item Evaluate the required derivatives $\frac{∂E_n}{∂w_{ji}} = δ_j z_i$.
		\end{itemize}
		$$\frac{∂E}{∂w_{ji}} = \sum_n \frac{∂E_n}{∂w_{ji}}$$
	\end{eBox}
	\includegraphics[width=\linewidth]{img/methods}
%\section{Unsupervised Methods}
%\subsection{Expectation Maximization Algorithms}
%	\begin{tBox}
%		\textbf{K-Means:}
%	\end{tBox}
\end{document}